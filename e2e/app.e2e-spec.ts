import { ReksioReduxPage } from './app.po';

describe('reksio-redux App', () => {
  let page: ReksioReduxPage;

  beforeEach(() => {
    page = new ReksioReduxPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
