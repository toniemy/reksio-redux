import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import {
    StoreRouterConnectingModule,
    RouterStateSerializer,
  } from '@ngrx/router-store';

import { AngularFireModule } from 'angularfire2';
import { firebaseConfig } from 'app/config';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import {
    CustomRouterStateSerializer,
    NavbarModule,
    SidebarModule
} from 'app/components';
import { reducers, metaReducers } from 'app/app-reducers';
import { AuthEffects } from 'app/state/auth/effects';
import { NewsEffects } from 'app/state/news/effects';
import { ONasEffects } from 'app/state/o-nas/effects';
import { DoctorsEffects } from 'app/state/doctors/effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        StoreModule.forRoot(reducers, { metaReducers }),
        StoreRouterConnectingModule,
        EffectsModule.forRoot([
            AuthEffects,
            NewsEffects,
            ONasEffects,
            DoctorsEffects
        ]),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        AppRoutingModule,
        NavbarModule,
        SidebarModule
    ],
    providers: [
        { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
