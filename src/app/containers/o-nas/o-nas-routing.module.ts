import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ONasComponent } from './o-nas.component';

const routes: Routes = [
    {
        path: '',
        component: ONasComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ONasRoutingModule { }
