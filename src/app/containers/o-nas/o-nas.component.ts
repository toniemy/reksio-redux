import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

import * as fromRoot from 'app/app-reducers';
import * as oNas from 'app/state/o-nas/actions';

@Component({
    templateUrl: './o-nas.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ONasComponent implements OnInit {
    oNasArticle$: Observable<string>;

    constructor(private store: Store<fromRoot.State>) {
        this.oNasArticle$ = store.select(fromRoot.getONasArticle);
    }

    ngOnInit() {
        this.store.dispatch(new oNas.ONasGetStartAction());
    }
}
