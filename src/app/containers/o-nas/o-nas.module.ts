import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ONasRoutingModule } from './o-nas-routing.module';
import { ONasComponent } from './o-nas.component';

@NgModule({
    imports: [
        CommonModule,
        ONasRoutingModule
    ],
    declarations: [ONasComponent]
})
export class ONasModule { }
