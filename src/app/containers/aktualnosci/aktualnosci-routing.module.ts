import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AktualnosciComponent } from './aktualnosci.component';

const routes: Routes = [
    {
        path: '',
        component: AktualnosciComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AktualnosciRoutingModule { }
