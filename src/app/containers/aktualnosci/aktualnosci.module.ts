import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AktualnosciRoutingModule } from './aktualnosci-routing.module';
import { AktualnosciComponent } from './aktualnosci.component';

@NgModule({
    imports: [
        CommonModule,
        AktualnosciRoutingModule
    ],
    declarations: [AktualnosciComponent]
})
export class AktualnosciModule { }
