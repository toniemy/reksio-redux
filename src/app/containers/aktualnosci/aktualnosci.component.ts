import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { News } from 'app/models';
import * as fromRoot from 'app/app-reducers';
import * as news from 'app/state/news/actions';

@Component({
    templateUrl: './aktualnosci.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AktualnosciComponent implements OnInit {
    news$: Observable<News[]>;

    constructor(private store: Store<fromRoot.State>) {
        this.news$ = store.select(fromRoot.getNewsAllVisible);
    }

    ngOnInit() {
        this.store.dispatch(new news.NewsGetAllVisibleStartAction());
    }
}
