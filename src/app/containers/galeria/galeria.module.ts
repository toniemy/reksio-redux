import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GaleriaRoutingModule } from './galeria-routing.module';
import { GaleriaComponent } from './galeria.component';

@NgModule({
    imports: [
        CommonModule,
        GaleriaRoutingModule
    ],
    declarations: [GaleriaComponent]
})
export class GaleriaModule { }
