import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfertaRoutingModule } from './oferta-routing.module';
import { OfertaComponent } from './oferta.component';

@NgModule({
    imports: [
        CommonModule,
        OfertaRoutingModule
    ],
    declarations: [OfertaComponent]
})
export class OfertaModule { }
