import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';

import { User } from 'app/models';
import * as fromRoot from 'app/app-reducers';
import * as auth from 'app/state/auth/actions';

@Component({
    selector: 'app-logowanie',
    templateUrl: './logowanie.component.html'
})
export class LogowanieComponent implements OnInit {
    user$: Observable<User>;
    user: User;

    constructor(
        private store: Store<fromRoot.State>
    ) {
        this.user$ = this.store.select(fromRoot.getUser);
    }

    ngOnInit() {
        this.user$.subscribe(user => this.user = user);
    }

    login() {
        this.store.dispatch(new auth.UserLoginStartAction());
    }

    logout() {
        this.store.dispatch(new auth.UserLogoutStartAction());
    }
}
