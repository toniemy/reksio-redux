import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularFireModule } from 'angularfire2';
import { firebaseConfig } from 'app/config/firebase';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { StoreModule } from '@ngrx/store';
import { reducer } from 'app/app-reducers';

import { LogowanieComponent } from './logowanie.component';

describe('LogowanieComponent', () => {
    let component: LogowanieComponent;
    let fixture: ComponentFixture<LogowanieComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AngularFireModule.initializeApp(firebaseConfig),
                AngularFireAuthModule,
                StoreModule.provideStore(reducer)
            ],
            declarations: [
                LogowanieComponent
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LogowanieComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
