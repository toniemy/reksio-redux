import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogowanieRoutingModule } from './logowanie-routing.module';
import { LogowanieComponent } from './logowanie.component';

@NgModule({
    imports: [
        CommonModule,
        LogowanieRoutingModule
    ],
    declarations: [
        LogowanieComponent
    ]
})
export class LogowanieModule { }
