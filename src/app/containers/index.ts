export { AktualnosciModule } from './aktualnosci/aktualnosci.module';
export { GaleriaModule } from './galeria/galeria.module';
export { KontaktModule } from './kontakt/kontakt.module';
export { ONasModule } from './o-nas/o-nas.module';
export { OfertaModule } from './oferta/oferta.module';
export { SklepikModule } from './sklepik/sklepik.module';
export { LogowanieModule } from './logowanie/logowanie.module';
