import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatunkiComponent } from './gatunki.component';

describe('GatunkiComponent', () => {
    let component: GatunkiComponent;
    let fixture: ComponentFixture<GatunkiComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GatunkiComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GatunkiComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
