import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SklepikComponent } from './sklepik.component';
import { GatunkiComponent } from './gatunki/gatunki.component';
import { KategorieComponent } from './kategorie/kategorie.component';
import { ProduktyComponent } from './produkty/produkty.component';
import { ProduktComponent } from './produkt/produkt.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'gatunki',
        pathMatch: 'full'
    },
    {
        path: 'gatunki',
        component: GatunkiComponent
    },
    {
        path: 'kategorie',
        component: KategorieComponent
    },
    {
        path: 'produkty',
        component: ProduktyComponent
    },
    {
        path: 'produkt',
        component: ProduktComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SklepikRoutingModule { }
