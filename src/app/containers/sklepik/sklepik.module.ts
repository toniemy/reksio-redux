import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SklepikRoutingModule } from './sklepik-routing.module';
import { SklepikComponent } from './sklepik.component';
import { GatunkiComponent } from './gatunki/gatunki.component';
import { KategorieComponent } from './kategorie/kategorie.component';
import { ProduktyComponent } from './produkty/produkty.component';
import { ProduktComponent } from './produkt/produkt.component';

@NgModule({
    imports: [
        CommonModule,
        SklepikRoutingModule
    ],
    declarations: [
        SklepikComponent,
        GatunkiComponent,
        KategorieComponent,
        ProduktyComponent,
        ProduktComponent
    ]
})
export class SklepikModule { }
