import { Action } from '@ngrx/store';

export const ONAS_GET_START = '[O Nas] Get start';
export const ONAS_GET_SUCCESS = '[O Nas] Get success';
export const ONAS_GET_FAILED = '[O Nas] Get failed';
export const ONAS_SAVE_START = '[O Nas] Save start';
export const ONAS_SAVE_SUCCESS = '[O Nas] Save success';
export const ONAS_SAVE_FAILED = '[O Nas] Save failed';

export class ONasGetStartAction implements Action {
    readonly type = ONAS_GET_START;

    constructor() { }
}

export class ONasGetSuccessAction implements Action {
    readonly type = ONAS_GET_SUCCESS;

    constructor(public payload: any) { }
}

export class ONasGetFailedAction implements Action {
    readonly type = ONAS_GET_FAILED;
}

export class ONasSaveStartAction implements Action {
    readonly type = ONAS_SAVE_START;

    constructor(public payload: any) { }
}

export class ONasSaveSuccessAction implements Action {
    readonly type = ONAS_SAVE_SUCCESS;
}

export class ONasSaveFailedAction implements Action {
    readonly type = ONAS_SAVE_FAILED;
}

export type Actions
    = ONasGetStartAction
    | ONasGetSuccessAction
    | ONasGetFailedAction
    | ONasSaveStartAction
    | ONasSaveSuccessAction
    | ONasSaveFailedAction;
