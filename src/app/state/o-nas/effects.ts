import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';

import {
    AngularFireDatabase
} from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

import * as actions from 'app/state/o-nas/actions';

@Injectable()
export class ONasEffects {

    @Effect()
    oNasGetStart$: Observable<Action> = this.actions$
        .ofType(actions.ONAS_GET_START)
        .switchMap(() =>
            this.db.object('/o-nas').valueChanges()
                .map((ONasState) => {
                    return new actions.ONasGetSuccessAction(ONasState);
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.ONasGetFailedAction());
                })
        );

    @Effect()
    oNasSaveStart$: Observable<Action> = this.actions$
        .ofType(actions.ONAS_SAVE_START)
        .map(toPayload)
        .switchMap(formValue =>
            Observable.fromPromise(this.db.object('/o-nas').set(formValue))
                .map(() => {
                    return new actions.ONasSaveSuccessAction();
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.ONasSaveFailedAction());
                })
        );

    constructor(
        private actions$: Actions,
        private db: AngularFireDatabase
    ) { }
}
