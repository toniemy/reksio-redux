import * as actions from 'app/state/o-nas/actions';

export interface State {
    article: string;
}

export const initialState: State = {
    article: ''
};

export function reducer(state = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.ONAS_GET_SUCCESS:
            return {
                ...state,
                ...action.payload
            };

        default:
            return state;
    }
}

export const getONas = (state: State) => state;
export const getONasArticle = (state: State) => state.article;
