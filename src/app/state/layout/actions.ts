import { Action } from '@ngrx/store';

export const OPEN_MOBILE_MENU = '[Layout] Open Mobile Menu';
export const CLOSE_MOBILE_MENU = '[Layout] Close Mobile Menu';

export class OpenMobileMenuAction implements Action {
    readonly type = OPEN_MOBILE_MENU;
}

export class CloseMobileMenuAction implements Action {
    readonly type = CLOSE_MOBILE_MENU;
}

export type Actions
    = OpenMobileMenuAction
    | CloseMobileMenuAction;
