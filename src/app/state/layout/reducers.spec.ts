import * as reducers from 'app/state/layout/reducers';
import * as actions from 'app/state/layout/actions';

describe('LayoutReducer', () => {
    describe('undefined action', () => {
        it('should return the default state', () => {
            const action = {} as any;

            const result = reducers.reducer(undefined, action);
            expect(result).toEqual(reducers.initialState);
        });
    });

    describe('OpenMobileMenuAction', () => {
        it('should return the OPEN_MOBILE_MENU state', () => {
            const expectedState = {
                showMobileMenu: true
            };

            const result = reducers.reducer(reducers.initialState, new actions.OpenMobileMenuAction());
            expect(result).toEqual(expectedState);
        });
    });

    describe('CloseMobileMenuAction', () => {
        it('should return the CLOSE_MOBILE_MENU state', () => {
            const expectedState = {
                showMobileMenu: false
            };

            const result = reducers.reducer(reducers.initialState, new actions.CloseMobileMenuAction());
            expect(result).toEqual(expectedState);
        });
    });
});
