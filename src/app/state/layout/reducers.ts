import * as actions from 'app/state/layout/actions';

export interface State {
    showMobileMenu: boolean;
}

export const initialState: State = {
    showMobileMenu: false
};

export function reducer(state = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.CLOSE_MOBILE_MENU:
            return {
                showMobileMenu: false
            };

        case actions.OPEN_MOBILE_MENU:
            return {
                showMobileMenu: true
            };

        default:
            return state;
    }
}

export const getShowMobileMenu = (state: State) => state.showMobileMenu;
