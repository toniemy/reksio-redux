import { Action } from '@ngrx/store';

import { News, FirebaseNewsEdited } from 'app/models';

export const NEWS_ADD_START = '[News] Add start';
export const NEWS_ADD_SUCCESS = '[News] Add success';
export const NEWS_ADD_FAILED = '[News] Add failed';
export const NEWS_EDIT_START = '[News] Edit start';
export const NEWS_EDIT_SUCCESS = '[News] Edit success';
export const NEWS_EDIT_FAILED = '[News] Edit failed';
export const NEWS_DELETE_START = '[News] Delete start';
export const NEWS_DELETE_SUCCESS = '[News] Delete success';
export const NEWS_DELETE_FAILED = '[News] Delete failed';
export const NEWS_GET_ALL_START = '[News] Get all start';
export const NEWS_GET_ALL_SUCCESS = '[News] Get all success';
export const NEWS_GET_ALL_FAILED = '[News] Get all failed';
export const NEWS_GET_ALL_VISIBLE_START = '[News] Get all visible start';
export const NEWS_GET_ALL_VISIBLE_SUCCESS = '[News] Get all visible success';
export const NEWS_GET_ALL_VISIBLE_FAILED = '[News] Get all visible failed';
export const NEWS_GET_SELECTED_START = '[News] Get selected start';
export const NEWS_GET_SELECTED_SUCCESS = '[News] Get selected success';
export const NEWS_GET_SELECTED_FAILED = '[News] Get selected failed';

export class NewsAddStartAction implements Action {
    readonly type = NEWS_ADD_START;

    constructor(public payload: News) { }
}

export class NewsAddSuccessAction implements Action {
    readonly type = NEWS_ADD_SUCCESS;
}

export class NewsAddFailedAction implements Action {
    readonly type = NEWS_ADD_FAILED;
}

export class NewsEditStartAction implements Action {
    readonly type = NEWS_EDIT_START;

    constructor(public payload: FirebaseNewsEdited) { }
}

export class NewsEditSuccessAction implements Action {
    readonly type = NEWS_EDIT_SUCCESS;
}

export class NewsEditFailedAction implements Action {
    readonly type = NEWS_EDIT_FAILED;
}

export class NewsDeleteStartAction implements Action {
    readonly type = NEWS_DELETE_START;

    constructor(public payload: string) { }
}

export class NewsDeleteSuccessAction implements Action {
    readonly type = NEWS_DELETE_SUCCESS;
}

export class NewsDeleteFailedAction implements Action {
    readonly type = NEWS_DELETE_FAILED;
}

export class NewsGetAllStartAction implements Action {
    readonly type = NEWS_GET_ALL_START;
}

export class NewsGetAllSuccessAction implements Action {
    readonly type = NEWS_GET_ALL_SUCCESS;

    constructor(public payload: News[]) { }
}

export class NewsGetAllFailedAction implements Action {
    readonly type = NEWS_GET_ALL_FAILED;
}

export class NewsGetAllVisibleStartAction implements Action {
    readonly type = NEWS_GET_ALL_VISIBLE_START;
}

export class NewsGetAllVisibleSuccessAction implements Action {
    readonly type = NEWS_GET_ALL_VISIBLE_SUCCESS;

    constructor(public payload: News[]) { }
}

export class NewsGetAllVisibleFailedAction implements Action {
    readonly type = NEWS_GET_ALL_VISIBLE_FAILED;
}

export class NewsGetSelectedStartAction implements Action {
    readonly type = NEWS_GET_SELECTED_START;

    constructor(public payload: string) { }
}

export class NewsGetSelectedSuccessAction implements Action {
    readonly type = NEWS_GET_SELECTED_SUCCESS;

    constructor(public payload: News) { }
}

export class NewsGetSelectedFailedAction implements Action {
    readonly type = NEWS_GET_SELECTED_FAILED;
}

export type Actions
    = NewsAddStartAction
    | NewsAddSuccessAction
    | NewsAddFailedAction
    | NewsEditStartAction
    | NewsEditSuccessAction
    | NewsEditFailedAction
    | NewsDeleteStartAction
    | NewsDeleteSuccessAction
    | NewsDeleteFailedAction
    | NewsGetAllStartAction
    | NewsGetAllSuccessAction
    | NewsGetAllFailedAction
    | NewsGetAllVisibleStartAction
    | NewsGetAllVisibleSuccessAction
    | NewsGetAllVisibleFailedAction
    | NewsGetSelectedStartAction
    | NewsGetSelectedSuccessAction
    | NewsGetSelectedFailedAction;
