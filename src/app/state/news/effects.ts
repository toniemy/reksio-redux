import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';

import {
    AngularFireDatabase
} from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

import * as actions from 'app/state/news/actions';
import { News } from 'app/models';

@Injectable()
export class NewsEffects {

    @Effect()
    newsAddStart$: Observable<Action> = this.actions$
        .ofType(actions.NEWS_ADD_START)
        .map(toPayload)
        .switchMap(formValue =>
            Observable.fromPromise(this.db.list('/news').push(formValue))
                .map(() => {
                    return new actions.NewsAddSuccessAction();
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.NewsAddFailedAction());
                })
        );

    @Effect()
    newsEditStart$: Observable<Action> = this.actions$
        .ofType(actions.NEWS_EDIT_START)
        .map(toPayload)
        .switchMap(item =>
            Observable.fromPromise(this.db.list('/news').update(item.$key, item.news))
                .map(() => {
                    return new actions.NewsEditSuccessAction();
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.NewsEditFailedAction());
                })
        );

    @Effect()
    newsDeleteStart$: Observable<Action> = this.actions$
        .ofType(actions.NEWS_DELETE_START)
        .map(toPayload)
        .switchMap(key =>
            Observable.fromPromise(this.db.list('/news').remove(key))
                .map(() => {
                    return new actions.NewsDeleteSuccessAction();
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.NewsDeleteFailedAction());
                })
        );

    @Effect()
    newsGetAll$ = this.actions$
        .ofType(actions.NEWS_GET_ALL_START)
        .switchMap(() =>
            this.db.list('/news', ref => ref.orderByChild('publishDate')).valueChanges()
                .map((list: News[]) => {
                    const filtered = list.reverse();
                    return new actions.NewsGetAllSuccessAction(filtered);
                })
                .catch((error): Observable<Action> => {
                    console.log(error);
                    return of(new actions.NewsGetAllFailedAction());
                })
        );

    @Effect()
    newsGetAllVisible$ = this.actions$
        .ofType(actions.NEWS_GET_ALL_VISIBLE_START)
        .switchMap(() =>
            this.db.list('/news', ref => ref.orderByChild('publishDate')).valueChanges()
                .map((list: News[]) => {
                    const filtered = list.filter(item => item.isHidden !== true).reverse();
                    return new actions.NewsGetAllVisibleSuccessAction(filtered);
                })
                .catch((error): Observable<Action> => {
                    console.log(error);
                    return of(new actions.NewsGetAllVisibleFailedAction());
                })
        );

    @Effect()
    newsGetSelected$ = this.actions$
        .ofType(actions.NEWS_GET_SELECTED_START)
        .map(toPayload)
        .switchMap(key =>
            this.db.list('/news', ref => ref.orderByKey().equalTo(key)).valueChanges()
                .map((list: News[]) => {
                    return new actions.NewsGetSelectedSuccessAction(list[0]);
                })
                .catch((error): Observable<Action> => {
                    console.log(error);
                    return of(new actions.NewsGetSelectedFailedAction());
                })
        );

    constructor(
        private actions$: Actions,
        private db: AngularFireDatabase
    ) { }
}
