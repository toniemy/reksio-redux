import * as actions from 'app/state/news/actions';
import { News } from 'app/models';

export interface State {
    all?: News[];
    visible?: News[];
    selected?: News;
}

export const initialState: State = {
    all: [],
    visible: []
};

export function reducer(state = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.NEWS_GET_ALL_SUCCESS:
            return {
                ...state,
                all: action.payload
            };

        case actions.NEWS_GET_ALL_VISIBLE_SUCCESS:
            return {
                ...state,
                visible: action.payload
            };

        case actions.NEWS_GET_SELECTED_SUCCESS:
            return {
                ...state,
                selected: action.payload
            };

        default:
            return state;
    }
}

export const getNewsAll = (state: State) => state.all;
export const getNewsAllVisible = (state: State) => state.visible;
export const getNewsSelected = (state: State) => state.selected;
