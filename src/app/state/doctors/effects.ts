import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from '@ngrx/effects';

import {
    AngularFireDatabase
} from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';

import * as actions from 'app/state/doctors/actions';
import { Doctor } from 'app/models';

@Injectable()
export class DoctorsEffects {

    @Effect()
    doctorAddStart$: Observable<Action> = this.actions$
        .ofType(actions.DOCTOR_ADD_START)
        .map(toPayload)
        .switchMap(formValue =>
            Observable.fromPromise(this.db.list('/doctors').push(formValue))
                .map(() => {
                    return new actions.DoctorAddSuccessAction();
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.DoctorAddFailedAction());
                })
        );

    @Effect()
    doctorEditStart$: Observable<Action> = this.actions$
        .ofType(actions.DOCTOR_EDIT_START)
        .map(toPayload)
        .switchMap(item =>
            Observable.fromPromise(this.db.list('/doctors').update(item.$key, item.news))
                .map(() => {
                    return new actions.DoctorEditSuccessAction();
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.DoctorEditFailedAction());
                })
        );

    @Effect()
    doctorDeleteStart$: Observable<Action> = this.actions$
        .ofType(actions.DOCTOR_DELETE_START)
        .map(toPayload)
        .switchMap(key =>
            Observable.fromPromise(this.db.list('/doctors').remove(key))
                .map(() => {
                    return new actions.DoctorDeleteSuccessAction();
                })
                .catch(error => {
                    console.log(error);
                    return of(new actions.DoctorDeleteFailedAction());
                })
        );

    @Effect()
    doctorGetAll$ = this.actions$
        .ofType(actions.DOCTOR_GET_ALL_START)
        .switchMap(() =>
            this.db.list('/doctors').valueChanges()
                .map((list: Doctor[]) => {
                    return new actions.DoctorGetAllSuccessAction(list);
                })
                .catch((error): Observable<Action> => {
                    console.log(error);
                    return of(new actions.DoctorGetAllFailedAction());
                })
        );

    @Effect()
    doctorGetAllVisible$ = this.actions$
        .ofType(actions.DOCTOR_GET_ALL_VISIBLE_START)
        .switchMap(() =>
            this.db.list('/doctors').valueChanges()
                .map((list: Doctor[]) => {
                    const filtered = list.filter(item => item.isHidden !== true);
                    return new actions.DoctorGetAllVisibleSuccessAction(filtered);
                })
                .catch((error): Observable<Action> => {
                    console.log(error);
                    return of(new actions.DoctorGetAllVisibleFailedAction());
                })
        );

    @Effect()
    doctorGetSelected$ = this.actions$
        .ofType(actions.DOCTOR_GET_SELECTED_START)
        .map(toPayload)
        .switchMap(key =>
            this.db.list('/doctors', ref => ref.orderByKey().equalTo(key)).valueChanges()
                .map((list: Doctor[]) => {
                    return new actions.DoctorGetSelectedSuccessAction(list[0]);
                })
                .catch((error): Observable<Action> => {
                    console.log(error);
                    return of(new actions.DoctorGetSelectedFailedAction());
                })
        );

    constructor(
        private actions$: Actions,
        private db: AngularFireDatabase
    ) { }
}
