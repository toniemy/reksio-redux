import * as actions from 'app/state/doctors/actions';
import { Doctor } from 'app/models';

export interface State {
    all?: Doctor[];
    visible?: Doctor[];
    selected?: Doctor;
}

export const initialState: State = {
    all: [],
    visible: []
};

export function reducer(state = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.DOCTOR_GET_ALL_SUCCESS:
            return {
                ...state,
                all: action.payload
            };

        case actions.DOCTOR_GET_ALL_VISIBLE_SUCCESS:
            return {
                ...state,
                visible: action.payload
            };

        case actions.DOCTOR_GET_SELECTED_SUCCESS:
            return {
                ...state,
                selected: action.payload
            };

        default:
            return state;
    }
}

export const getDoctorAll = (state: State) => state.all;
export const getDoctorAllVisible = (state: State) => state.visible;
export const getDoctorSelected = (state: State) => state.selected;
