import { Action } from '@ngrx/store';

import { Doctor, FirebaseDoctorEdited } from 'app/models';

export const DOCTOR_ADD_START = '[Doctor] Add start';
export const DOCTOR_ADD_SUCCESS = '[Doctor] Add success';
export const DOCTOR_ADD_FAILED = '[Doctor] Add failed';
export const DOCTOR_EDIT_START = '[Doctor] Edit start';
export const DOCTOR_EDIT_SUCCESS = '[Doctor] Edit success';
export const DOCTOR_EDIT_FAILED = '[Doctor] Edit failed';
export const DOCTOR_DELETE_START = '[Doctor] Delete start';
export const DOCTOR_DELETE_SUCCESS = '[Doctor] Delete success';
export const DOCTOR_DELETE_FAILED = '[Doctor] Delete failed';
export const DOCTOR_GET_ALL_START = '[Doctor] Get all start';
export const DOCTOR_GET_ALL_SUCCESS = '[Doctor] Get all success';
export const DOCTOR_GET_ALL_FAILED = '[Doctor] Get all failed';
export const DOCTOR_GET_ALL_VISIBLE_START = '[Doctor] Get all visible start';
export const DOCTOR_GET_ALL_VISIBLE_SUCCESS = '[Doctor] Get all visible success';
export const DOCTOR_GET_ALL_VISIBLE_FAILED = '[Doctor] Get all visible failed';
export const DOCTOR_GET_SELECTED_START = '[Doctor] Get selected start';
export const DOCTOR_GET_SELECTED_SUCCESS = '[Doctor] Get selected success';
export const DOCTOR_GET_SELECTED_FAILED = '[Doctor] Get selected failed';

export class DoctorAddStartAction implements Action {
    readonly type = DOCTOR_ADD_START;

    constructor(public payload: Doctor) { }
}

export class DoctorAddSuccessAction implements Action {
    readonly type = DOCTOR_ADD_SUCCESS;
}

export class DoctorAddFailedAction implements Action {
    readonly type = DOCTOR_ADD_FAILED;
}

export class DoctorEditStartAction implements Action {
    readonly type = DOCTOR_EDIT_START;

    constructor(public payload: FirebaseDoctorEdited) { }
}

export class DoctorEditSuccessAction implements Action {
    readonly type = DOCTOR_EDIT_SUCCESS;
}

export class DoctorEditFailedAction implements Action {
    readonly type = DOCTOR_EDIT_FAILED;
}

export class DoctorDeleteStartAction implements Action {
    readonly type = DOCTOR_DELETE_START;

    constructor(public payload: string) { }
}

export class DoctorDeleteSuccessAction implements Action {
    readonly type = DOCTOR_DELETE_SUCCESS;
}

export class DoctorDeleteFailedAction implements Action {
    readonly type = DOCTOR_DELETE_FAILED;
}

export class DoctorGetAllStartAction implements Action {
    readonly type = DOCTOR_GET_ALL_START;
}

export class DoctorGetAllSuccessAction implements Action {
    readonly type = DOCTOR_GET_ALL_SUCCESS;

    constructor(public payload: Doctor[]) { }
}

export class DoctorGetAllFailedAction implements Action {
    readonly type = DOCTOR_GET_ALL_FAILED;
}

export class DoctorGetAllVisibleStartAction implements Action {
    readonly type = DOCTOR_GET_ALL_VISIBLE_START;
}

export class DoctorGetAllVisibleSuccessAction implements Action {
    readonly type = DOCTOR_GET_ALL_VISIBLE_SUCCESS;

    constructor(public payload: Doctor[]) { }
}

export class DoctorGetAllVisibleFailedAction implements Action {
    readonly type = DOCTOR_GET_ALL_VISIBLE_FAILED;
}

export class DoctorGetSelectedStartAction implements Action {
    readonly type = DOCTOR_GET_SELECTED_START;

    constructor(public payload: string) { }
}

export class DoctorGetSelectedSuccessAction implements Action {
    readonly type = DOCTOR_GET_SELECTED_SUCCESS;

    constructor(public payload: Doctor) { }
}

export class DoctorGetSelectedFailedAction implements Action {
    readonly type = DOCTOR_GET_SELECTED_FAILED;
}

export type Actions
    = DoctorAddStartAction
    | DoctorAddSuccessAction
    | DoctorAddFailedAction
    | DoctorEditStartAction
    | DoctorEditSuccessAction
    | DoctorEditFailedAction
    | DoctorDeleteStartAction
    | DoctorDeleteSuccessAction
    | DoctorDeleteFailedAction
    | DoctorGetAllStartAction
    | DoctorGetAllSuccessAction
    | DoctorGetAllFailedAction
    | DoctorGetAllVisibleStartAction
    | DoctorGetAllVisibleSuccessAction
    | DoctorGetAllVisibleFailedAction
    | DoctorGetSelectedStartAction
    | DoctorGetSelectedSuccessAction
    | DoctorGetSelectedFailedAction;
