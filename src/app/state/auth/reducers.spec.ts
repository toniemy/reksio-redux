import * as reducers from 'app/state/auth/reducers';
import * as actions from 'app/state/auth/actions';

describe('UserReducer', () => {
    describe('undefined action', () => {
        it('should return the default state', () => {
            const action = {} as any;

            const result = reducers.reducer(undefined, action);
            expect(result).toEqual(reducers.initialState);
        });
    });

    describe('UserLoginSuccessAction', () => {
        it('should return the USER_LOGIN_SUCCESS state', () => {
            const credentialsMock = {
                emailVerified: true,
                loggedIn: true,
                name: 'Dawid Lubowiecki',
                additionalInfo: 'test'
            };
            const expectedState = {
                details: credentialsMock
            };

            const result = reducers.reducer(reducers.initialState, new actions.UserLoginSuccessAction(credentialsMock));
            expect(result).toEqual(expectedState);
        });
    });

    describe('UserLogoutSuccessAction', () => {
        it('should return the USER_LOGOUT_SUCCESS state', () => {
            const result = reducers.reducer(reducers.initialState, new actions.UserLogoutSuccessAction());
            expect(result).toEqual(reducers.initialState);
        });
    });
});
