import { Action } from '@ngrx/store';

import { User } from 'app/models';

export const USER_LOGIN_START = '[User] Login start';
export const USER_LOGIN_SUCCESS = '[User] Login success';
export const USER_LOGIN_FAILED = '[User] Login failed';
export const USER_LOGOUT_START = '[User] Logout start';
export const USER_LOGOUT_SUCCESS = '[User] Logout success';

export class UserLoginStartAction implements Action {
    readonly type = USER_LOGIN_START;
}

export class UserLoginSuccessAction implements Action {
    readonly type = USER_LOGIN_SUCCESS;

    constructor(public payload: User) { }
}

export class UserLoginFailedAction implements Action {
    readonly type = USER_LOGIN_FAILED;
}

export class UserLogoutStartAction implements Action {
    readonly type = USER_LOGOUT_START;
}

export class UserLogoutSuccessAction implements Action {
    readonly type = USER_LOGOUT_SUCCESS;
}

export type Actions
    = UserLoginStartAction
    | UserLoginSuccessAction
    | UserLoginFailedAction
    | UserLogoutStartAction
    | UserLogoutSuccessAction;
