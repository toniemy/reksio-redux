import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';

import * as auth from 'app/state/auth/actions';
import { User } from 'app/models';

@Injectable()
export class AuthEffects {

    @Effect()
    authLoginStart$: Observable<Action> = this.actions$
        .ofType(auth.USER_LOGIN_START)
        .switchMap(() =>
            Observable
                .fromPromise(this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()))
                .map(user => {
                    const userDetail: User = {
                        emailVerified: user.user.emailVerified,
                        loggedIn: true,
                        name: user.user.providerData[0].displayName
                    };
                    return new auth.UserLoginSuccessAction(userDetail);
                })
                .catch(error => {
                    console.log(error);
                    return of(new auth.UserLoginFailedAction());
                })
        );

    @Effect()
    authLogout$: Observable<Action> = this.actions$
        .ofType(auth.USER_LOGOUT_START)
        .switchMap(() =>
            Observable.fromPromise(this.afAuth.auth.signOut())
                .map(() => new auth.UserLogoutSuccessAction())
        );

    constructor(
        private actions$: Actions,
        private afAuth: AngularFireAuth
    ) { }
}
