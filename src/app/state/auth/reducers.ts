import * as auth from 'app/state/auth/actions';
import { User } from 'app/models';

export interface State {
    details: User;
}

export const initialState: State = {
    details: {
        emailVerified: false,
        loggedIn: false,
        name: ''
    }
};

export function reducer(state = initialState, action: auth.Actions): State {
    switch (action.type) {
        case auth.USER_LOGIN_SUCCESS:
            return {
                details: action.payload
            };

        case auth.USER_LOGIN_FAILED:
        case auth.USER_LOGOUT_SUCCESS:
            return initialState;

        default:
            return state;
    }
}

export const getUser = (state: State) => state.details;
