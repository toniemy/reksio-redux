export interface User {
    emailVerified: boolean;
    loggedIn: boolean;
    name: string;
}
