export interface Doctor {
    name: string;
    photo: string;
    content: string;
    isHidden: boolean;
}
