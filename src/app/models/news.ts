export interface News {
    title: string;
    content: string;
    isHidden: boolean;
    publishDate: number;
}
