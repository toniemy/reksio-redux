import { News, Doctor } from 'app/models';

export interface FirebaseNewsEdited {
    $key: string;
    news: News;
}

export interface FirebaseDoctorEdited {
    $key: string;
    doctor: Doctor;
}
