import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'aktualnosci',
        pathMatch: 'full'
    },
    {
        path: 'aktualnosci',
        loadChildren: './containers-panel/aktualnosci/aktualnosci.module#PanelAktualnosciModule'
    },
    {
        path: 'o-nas',
        loadChildren: './containers-panel/o-nas/o-nas.module#PanelONasModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PanelRoutingModule {}
