import { NgModule } from '@angular/core';

import { PanelRoutingModule } from './panel-routing.module';

@NgModule({
    imports: [
        PanelRoutingModule
    ],
    declarations: []
})
export class PanelModule { }
