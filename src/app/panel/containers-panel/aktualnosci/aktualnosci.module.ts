import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AktualnosciRoutingModule } from './aktualnosci-routing.module';
import { AktualnosciComponent } from './aktualnosci.component';
import { AktualnoscEdycjaComponent } from './aktualnosc-edycja/aktualnosc-edycja.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AktualnosciRoutingModule
    ],
    declarations: [
        AktualnosciComponent,
        AktualnoscEdycjaComponent
    ]
})
export class PanelAktualnosciModule { }
