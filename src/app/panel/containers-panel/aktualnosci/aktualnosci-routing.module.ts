import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AktualnosciComponent } from './aktualnosci.component';
import { AktualnoscEdycjaComponent } from './aktualnosc-edycja/aktualnosc-edycja.component';

const routes: Routes = [
    {
        path: '',
        component: AktualnosciComponent
    },
    {
        path: ':id',
        component: AktualnoscEdycjaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AktualnosciRoutingModule { }
