import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

import { News } from 'app/models';
import * as fromRoot from 'app/app-reducers';
import * as news from 'app/state/news/actions';

@Component({
    templateUrl: './aktualnosci.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AktualnosciComponent implements OnInit {
    @ViewChild('newsForm') newsForm;
    news$: Observable<News[]>;

    constructor(
        private store: Store<fromRoot.State>,
        private router: Router
    ) {
        this.news$ = store.select(fromRoot.getNewsAll);
    }

    ngOnInit() {
        this.store.dispatch(new news.NewsGetAllStartAction());
    }

    add(formValue) {
        const {title, content, isHidden} = formValue;
        if (!title) { return; }
        const newNews: News = {
            title,
            content,
            isHidden: isHidden || false,
            publishDate: new Date().getTime()
        };
        this.store.dispatch(new news.NewsAddStartAction(newNews));
        this.newsForm.reset();
    }

    edit(key) {
        this.router.navigate(['/panel/aktualnosci', key]);
    }

    delete(key) {
        this.store.dispatch(new news.NewsDeleteStartAction(key));
    }
}
