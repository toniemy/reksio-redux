import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';

import { reducer } from 'app/app-reducers';
import { AktualnosciComponent } from './aktualnosci.component';

describe('AktualnosciComponent', () => {
    let component: AktualnosciComponent;
    let fixture: ComponentFixture<AktualnosciComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                StoreModule.provideStore(reducer)
            ],
            declarations: [
                AktualnosciComponent
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AktualnosciComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
