import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AktualnoscEdycjaComponent } from './aktualnosc-edycja.component';

describe('AktualnoscEdycjaComponent', () => {
  let component: AktualnoscEdycjaComponent;
  let fixture: ComponentFixture<AktualnoscEdycjaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AktualnoscEdycjaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AktualnoscEdycjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
