import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

import { News } from 'app/models';
import * as fromRoot from 'app/app-reducers';
import * as news from 'app/state/news/actions';

@Component({
    selector: 'app-aktualnosc-edycja',
    templateUrl: './aktualnosc-edycja.component.html'
})
export class AktualnoscEdycjaComponent implements OnInit {
    id: string;
    selected$: Observable<News>;
    selected: News;

    constructor(
        private store: Store<fromRoot.State>,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.id = route.snapshot.paramMap.get('id');
        this.selected$ = store.select(fromRoot.getNewsSelected);
    }

    ngOnInit() {
        this.store.dispatch(new news.NewsGetSelectedStartAction(this.id));
        this.selected$.subscribe(item => this.selected = item);
    }

    save(formValue) {
        const edited = {
            news: {
                ...this.selected,
                ...formValue
            },
            $key: this.id
        };
        this.store.dispatch(new news.NewsEditStartAction(edited));
        this.router.navigate(['/panel/aktualnosci']);
    }

    cancel() {
        this.router.navigate(['/panel/aktualnosci']);
    }

}
