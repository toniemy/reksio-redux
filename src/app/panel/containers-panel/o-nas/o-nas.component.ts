import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

import { Doctor } from 'app/models';
import * as fromRoot from 'app/app-reducers';
import * as doctors from 'app/state/doctors/actions';
import * as oNas from 'app/state/o-nas/actions';

@Component({
    templateUrl: './o-nas.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ONasComponent implements OnInit {
    @ViewChild('oNasForm') oNasForm;
    @ViewChild('doctorsForm') doctorsForm;

    oNasArticle$: Observable<string>;
    doctors$: Observable<Doctor[]>;

    constructor(
        private store: Store<fromRoot.State>,
        private router: Router
    ) {
        this.oNasArticle$ = store.select(fromRoot.getONasArticle);
        this.doctors$ = store.select(fromRoot.getDoctorAll);
    }

    ngOnInit() {
        this.store.dispatch(new doctors.DoctorGetAllStartAction());
        this.store.dispatch(new oNas.ONasGetStartAction());
    }

    addDoctor(formValue) {
        const {doctorName, doctorPhoto, doctorContent, doctorIsHidden} = formValue;
        if (!doctorName) { return; }
        const newDoctor: Doctor = {
            name: doctorName,
            photo: doctorPhoto,
            content: doctorContent,
            isHidden: doctorIsHidden || false
        };
        this.store.dispatch(new doctors.DoctorAddStartAction(newDoctor));
        this.doctorsForm.reset();
    }

    editDoctor(key) {
        this.router.navigate(['/panel/o-nas', key]);
    }

    deleteDoctor(key) {
        this.store.dispatch(new doctors.DoctorDeleteStartAction(key));
    }

    saveONas(formValue) {
        this.store.dispatch(new oNas.ONasSaveStartAction(formValue));
    }
}
