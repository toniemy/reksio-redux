import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ONasRoutingModule } from './o-nas-routing.module';
import { ONasComponent } from './o-nas.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ONasRoutingModule
    ],
    declarations: [
        ONasComponent
    ]
})
export class PanelONasModule { }
