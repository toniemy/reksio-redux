import { Link } from 'app/models';

export const MENU_LINKS: Link[] = [
    {
        label: 'Aktualności',
        url: 'aktualnosci'
    },
    {
        label: 'O nas',
        url: 'o-nas'
    },
    {
        label: 'Oferta',
        url: 'oferta'
    },
    {
        label: 'Sklep',
        url: 'sklep'
    },
    {
        label: 'Galeria',
        url: 'galeria'
    },
    {
        label: 'Kontakt',
        url: 'kontakt'
    }
];
