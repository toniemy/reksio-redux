import {
    ActionReducerMap,
    createSelector,
    createFeatureSelector,
    ActionReducer,
    MetaReducer,
} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { environment } from '../environments/environment';
import { RouterStateUrl } from 'app/components';
import { storeFreeze } from 'ngrx-store-freeze';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as fromLayout from 'app/state/layout/reducers';
import * as fromAuth from 'app/state/auth/reducers';
import * as fromNews from 'app/state/news/reducers';
import * as fromONas from 'app/state/o-nas/reducers';
import * as fromDoctors from 'app/state/doctors/reducers';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
    layout: fromLayout.State;
    auth: fromAuth.State;
    news: fromNews.State;
    oNas: fromONas.State;
    doctors: fromDoctors.State;
    router: fromRouter.RouterReducerState<RouterStateUrl>;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<State> = {
    layout: fromLayout.reducer,
    auth: fromAuth.reducer,
    news: fromNews.reducer,
    oNas: fromONas.reducer,
    doctors: fromDoctors.reducer,
    router: fromRouter.routerReducer
};

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<State>[] = !environment.production
    ? [storeFreeze]
    : [];

/**
 * Layout Reducers
 */
export const getLayoutState = (state: State) => state.layout;
export const getShowMobileMenu = createSelector(getLayoutState, fromLayout.getShowMobileMenu);

/**
 * Auth Reducers
 */
export const getAuthState = (state: State) => state.auth;
export const getUser = createSelector(getAuthState, fromAuth.getUser);

/**
 * News Reducers
 */
export const getNewsState = (state: State) => state.news;
export const getNewsAll = createSelector(getNewsState, fromNews.getNewsAll);
export const getNewsAllVisible = createSelector(getNewsState, fromNews.getNewsAllVisible);
export const getNewsSelected = createSelector(getNewsState, fromNews.getNewsSelected);

/**
 * ONas Reducers
 */
export const getONasState = (state: State) => state.oNas;
export const getONas = createSelector(getONasState, fromONas.getONas);
export const getONasArticle = createSelector(getONasState, fromONas.getONasArticle);

/**
 * Doctors Reducers
 */
export const getDoctorsState = (state: State) => state.doctors;
export const getDoctorAll = createSelector(getDoctorsState, fromDoctors.getDoctorAll);
export const getDoctorAllVisible = createSelector(getDoctorsState, fromDoctors.getDoctorAllVisible);
export const getDoctorSelected = createSelector(getDoctorsState, fromDoctors.getDoctorSelected);
