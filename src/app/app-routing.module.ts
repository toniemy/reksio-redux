import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'aktualnosci',
        pathMatch: 'full'
    },
    {
        path: 'aktualnosci',
        loadChildren: './containers/aktualnosci/aktualnosci.module#AktualnosciModule'
    },
    {
        path: 'o-nas',
        loadChildren: './containers/o-nas/o-nas.module#ONasModule'
    },
    {
        path: 'oferta',
        loadChildren: './containers/oferta/oferta.module#OfertaModule'
    },
    {
        path: 'sklep',
        loadChildren: './containers/sklepik/sklepik.module#SklepikModule'
    },
    {
        path: 'galeria',
        loadChildren: './containers/galeria/galeria.module#GaleriaModule'
    },
    {
        path: 'kontakt',
        loadChildren: './containers/kontakt/kontakt.module#KontaktModule'
    },
    {
        path: 'logowanie',
        loadChildren: './containers/logowanie/logowanie.module#LogowanieModule'
    },
    {
        path: 'panel',
        loadChildren: './panel/panel.module#PanelModule'
    },
    {
        path: '**',
        redirectTo: 'aktualnosci',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
