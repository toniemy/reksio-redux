import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';

import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    AfterViewInit
} from '@angular/core';
import {
    NavigationStart,
    Router
} from '@angular/router';
import { Store } from '@ngrx/store';

import * as fromRoot from 'app/app-reducers';
import * as layout from 'app/state/layout/actions';
import { MENU_LINKS } from 'app/config';
import { Link } from 'app/models';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements AfterViewInit {
    showMobileMenu$: Observable<boolean>;
    showMobileMenu: boolean;
    menuLinks: Link[] = MENU_LINKS;

    constructor(
        private router: Router,
        private store: Store<fromRoot.State>
    ) {
        this.showMobileMenu = false;
        this.showMobileMenu$ = this.store.select(fromRoot.getShowMobileMenu);
        this.menuLinks = MENU_LINKS;
    }

    ngAfterViewInit() {
        this.showMobileMenu$.subscribe(value => this.showMobileMenu = value);

        this.router.events
            .filter(event => event instanceof NavigationStart)
            .subscribe((event: NavigationStart) => {
                if (this.showMobileMenu) {
                    this.store.dispatch(new layout.CloseMobileMenuAction());
                }
            });
    }

    toggleMenu(showMobileMenu): void {
        showMobileMenu
            ? this.store.dispatch(new layout.CloseMobileMenuAction())
            : this.store.dispatch(new layout.OpenMobileMenuAction());
    }
}
