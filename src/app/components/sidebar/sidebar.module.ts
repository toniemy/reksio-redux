import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar.component';

@NgModule({
    imports: [],
    declarations: [SidebarComponent],
    exports: [SidebarComponent]
})
export class SidebarModule { }
